#!/usr/bin/env python

import pytest
from cusip import cusip

def test_cusip():
    cusips = [
        '126650BG4',
        '254709AC2',
        '437076AQ5',
        '441060AG5',
        '50075NAN4',
        '574599BE5',
        '617446B99',
        '637640AC7',
        '713291AL6',
        '852061AE0',
        '887317AA3',
        '925524BF6'
    ]

    for c in cusips:
        base = c[:8]
        assert cusip(base)[8] == c[8], "Error: {}".format(c)