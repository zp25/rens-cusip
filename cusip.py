#!/usr/bin/env python

import os

class CUSIPError(ValueError): pass

def cusip(base):
    s = 0
    for (count, i) in enumerate(base):
        r = ord(i)

        if r>=48 and r<=57:
            v = r - 48
        elif r>=65 and r<=90:
            v = r - 55
        elif r>=97 and r<=122:
            v = r - 87
        elif r==42:
            v = 36
        elif r==64:
            v = 37
        elif r==35:
            v = 38
        else:
            raise CUSIPError('Invalid CUSIP: {}'.format(base))

        if count in [1, 3, 5, 7]:
            v *= 2

        s += sum(divmod(v, 10))

    r = (10 - s%10) % 10
    rs = base + str(r)

    return rs


if __name__ == '__main__':
    inname = 'PERMNO_CUSIP.txt'
    outname = 'result.txt'

    DIR = 'data'

    infile = os.path.join(DIR, os.path.basename(inname))
    outfile = os.path.join(DIR, os.path.basename(outname))

    open(outfile, "w").close()

    try:
        with open(infile) as fp, open(outfile, 'a') as fpw:
            for line in fp:
                c = line.split()[1]

                r = cusip(c)
                fpw.write(r + '\n')

    except CUSIPError as err:
        print(err)

    else:
        print('Done!')